// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "TheStationCharacter.h"
#include "FinalDoor.h"


void AFinalDoor::NotifyActorBeginOverlap(AActor* Other)
{
	//enable widget in ui to show
	if (Other->GetClass()->IsChildOf(ATheStationCharacter::StaticClass()))
	{
		ATheStationCharacter* player = Cast<ATheStationCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (player)
		{
			if (ATheStationCharacter::hasKey == true)
			{
				player->showInteractMessage = true;
				player->door = this;
			}
		}
	}
}

void AFinalDoor::NotifyActorEndOverlap(AActor* Other)
{
	//hide widget in ui
	if (Other->GetClass()->IsChildOf(ATheStationCharacter::StaticClass()))
	{
		ATheStationCharacter* player = Cast<ATheStationCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (player)
		{
			if (ATheStationCharacter::hasKey == true)
			{
				player->door = nullptr;
				player->showInteractMessage = false;
			}
		}
	}
}