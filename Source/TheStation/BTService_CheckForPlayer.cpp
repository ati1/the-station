// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "TheStationCharacter.h"
#include "EnemyAI.h"
#include "EnemyCharacter.h"
#include "BTService_CheckForPlayer.h"

UBTService_CheckForPlayer::UBTService_CheckForPlayer()
{
	bCreateNodeInstance = true;
}

void UBTService_CheckForPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AEnemyAI* EnemyPC = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

	if (EnemyPC)
	{
		ATheStationCharacter *Enemy = Cast<ATheStationCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

		if(Enemy)
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyID, Enemy);

		FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, EnemyPC->GetPawn());
		RV_TraceParams.bTraceComplex = true;

		//TODO: This might need to be looked at?
		//RV_TraceParams.bTraceAsyncScene = true;
		RV_TraceParams.bReturnPhysicalMaterial = false;

		//Re-initialize hit info
		FHitResult RV_Hit(ForceInit);
		const FVector SpawnLocation = EnemyPC->GetPawn()->GetActorLocation();

		FVector Start = SpawnLocation;
		FVector End = Enemy->GetActorLocation() + 10;

		GetWorld()->LineTraceSingleByChannel(
			RV_Hit,        //result
			Start,    //start
			End, //end
			ECC_Pawn, //collision channel
			RV_TraceParams
		);

		/*DrawDebugLine(
			GetWorld(),
			Start,
			End,
			FColor(0, 255, 0),
			false, 3, 0,
			12.333
		);*/

		if (RV_Hit.bBlockingHit)
		{
			/*if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, RV_Hit.GetActor()->GetName());*/

			if (RV_Hit.GetActor() == Enemy)
			{
				//start to follow enemy
				OwnerComp.GetBlackboardComponent()->SetValueAsBool("IsSeeingEnemy", true);
			}
			else
			{
				//there is something between player and enemy, stop following
				OwnerComp.GetBlackboardComponent()->SetValueAsBool("IsSeeingEnemy", false);
			}
		}
	}

}
