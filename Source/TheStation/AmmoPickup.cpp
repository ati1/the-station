// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "TheStationCharacter.h"
#include "AmmoPickup.h"


// Sets default values
AAmmoPickup::AAmmoPickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	count = 5;
	TouchSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TouchSphereComponent"));
	TouchSphere->SetSphereRadius(45, false);
	TouchSphere->OnComponentBeginOverlap.AddDynamic(this, &AAmmoPickup::OnOverlapBegin);
	RootComponent = TouchSphere;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AAmmoPickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAmmoPickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AAmmoPickup::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ATheStationCharacter* player = Cast<ATheStationCharacter>(OtherActor);

	if (player)
	{
		player->AddAmmo(count);
		this->Destroy();
	}
}

