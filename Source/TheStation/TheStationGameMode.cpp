// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "TheStation.h"
#include "TheStationGameMode.h"
#include "TheStationHUD.h"
#include "TheStationCharacter.h"

ATheStationGameMode::ATheStationGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATheStationHUD::StaticClass();
	
}

void ATheStationGameMode::BeginPlay()
{
	if (ATheStationCharacter::lastLevel == "Level2")
	{
		APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		AActor* spot = FindPlayerStart(pc, ATheStationCharacter::lastLevel);
		pc->GetPawn()->SetActorLocation(spot->GetActorLocation());
	}
	else
	{
		//spawn on normal spot
		APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		AActor* spot = FindPlayerStart(pc, "Level1");
		if(!spot)
			spot = FindPlayerStart(pc, "None");

		pc->GetPawn()->SetActorLocation(spot->GetActorLocation());
	}
			
	ATheStationCharacter::lastLevel = UGameplayStatics::GetCurrentLevelName(GetWorld());
	
}




