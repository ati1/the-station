// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "TheStationCharacter.h"
#include "KeyItem.h"


// Sets default values
AKeyItem::AKeyItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	Trigger = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerCollider"));
	RootComponent = Trigger;

	Trigger->SetGenerateOverlapEvents(true);
	Trigger->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

}

// Called when the game starts or when spawned
void AKeyItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKeyItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AKeyItem::NotifyActorBeginOverlap(AActor* Other)
{
	if (Other->GetClass()->IsChildOf(ATheStationCharacter::StaticClass()))
	{
		ATheStationCharacter* player = Cast<ATheStationCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (player)
		{
			//set key true lel
			ATheStationCharacter::hasKey = true;
			this->Destroy();
		}
	}
}

