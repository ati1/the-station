// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Door.h"
#include "FinalDoor.generated.h"

/**
 * 
 */
UCLASS()
class THESTATION_API AFinalDoor : public ADoor
{
	GENERATED_BODY()


	virtual void NotifyActorBeginOverlap(AActor* Other) override;
	virtual void NotifyActorEndOverlap(AActor* Other) override;
	
};
