// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "TheStation.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"
#include "TheStationCharacter.h"
#include "TheStationProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "EnemyCharacter.h"
#include "Door.h"

/*#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"*/

#include "MotionControllerComponent.h"

#include "XRMotionControllerBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ATheStationCharacter

ATheStationCharacter::ATheStationCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	hitParticle = CreateDefaultSubobject<UParticleSystem>(TEXT("hitParticle"));
	bloodParticle = CreateDefaultSubobject<UParticleSystem>(TEXT("bloodParticle"));
	muzzleFlash = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("muzzleFlash"));

	maxLoadedAmmo = 10;
	maxAmmoPool = 50;

	loadedAmmo = maxLoadedAmmo;
	ammoPool = maxAmmoPool;

	ElapsedTimeMini = 0.f;
	TimerEnd = 0.1f;

	//static ConstructorHelpers::FObjectFinder<TypeOfComponent> TheObjectName(TEXT("<Insert Path here>"));

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FP_Gun"));
	//FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);
	//FP_Gun->RelativeLocation = FVector(-2.55f, 39.69f, 10.95f);
	//FP_Gun->RelativeRotation = FRotator(0, 0, 90);
	//FP_Gun->RelativeScale3D = FVector(2.5f, 2.5f, 2.5f);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	/*VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.*/

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	//muzzleFlare->AttachTo(VR_Gun);
	//muzzleFlare->SetWorldLocation(VR_MuzzleLocation->GetSocketLocation("VR_MuzzleLocation"));
	//muzzleFlare->SetRelativeLocation(FP_MuzzleLocation->GetComponentTransform().GetLocation());
	//muzzleFlare->SetRelativeLocation();

	//WidgetInstance = CreateWidget(UGameplayStatics::GetGameInstance(GetWorld()), WidgetTemplate);

}

//ATheStationCharacter::hasKey;
bool ATheStationCharacter::hasKey = false;

FString ATheStationCharacter::lastLevel = "";

void ATheStationCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));


	//Set gun model to right position
	FP_Gun->RelativeLocation = FVector(1, 26, 6);
	FP_Gun->RelativeRotation = FRotator(0, 0, 90);
	FP_Gun->RelativeScale3D = FVector(27, 27, 27);

	muzzleFlash->AttachToComponent(FP_Gun, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true));

	//set muzzleflash location
	muzzleFlash->RelativeLocation = FVector(0.05f, 0, 2);
	muzzleFlash->RelativeRotation = FRotator(90, 0, 0);
	muzzleFlash->RelativeScale3D = FVector(0.03f, 0.03f, 0.03f);


	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		//VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		//VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	CurHealth = MaxHealth;
	canShoot = true;
}

void ATheStationCharacter::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	if (shooting)
	{
		ElapsedTimeMini += deltaTime;
		if (ElapsedTimeMini >= TimerEnd)
		{
			ATheStationCharacter::OnFire();
			ElapsedTimeMini = 0;
		}
	}
	//GEngine->AddOnScreenDebugMessage(0, deltaTime, FColor::Red, FString::Printf(TEXT("HP: %f"), CurHealth));
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATheStationCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATheStationCharacter::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("FireBegin", IE_Pressed, this, &ATheStationCharacter::OnBeginShoot);
		PlayerInputComponent->BindAction("FireRelease", IE_Released, this, &ATheStationCharacter::OnEndShoot);
		//PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ATheStationCharacter::OnFire);
	}

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ATheStationCharacter::Interact);

	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &ATheStationCharacter::Pause).bExecuteWhenPaused = true;

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ATheStationCharacter::OnReload);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ATheStationCharacter::OnResetVR);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATheStationCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATheStationCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATheStationCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATheStationCharacter::LookUpAtRate);
}

void ATheStationCharacter::Interact()
{
	if (door != nullptr)
	{
		door->OpenDoor();
	}
}

void ATheStationCharacter::Pause()
{
	APlayerController* PC = Cast<APlayerController>(GetController());

	if (!isPaused)
	{
		isPaused = true;
		UGameplayStatics::SetGamePaused(GetWorld(), true);
		if (PC)
		{
			PC->bShowMouseCursor = true;
			PC->bEnableClickEvents = true;
			PC->bEnableMouseOverEvents = true;
		}
		//show pause menu

		//WidgetInstance->SetVisibility(EVisibility::Visible);
	}
	else
	{
		isPaused = false;
		UGameplayStatics::SetGamePaused(GetWorld(), false);
		if (PC)
		{
			PC->bShowMouseCursor = false;
			PC->bEnableClickEvents = false;
			PC->bEnableMouseOverEvents = false;
		}
		//hide pause menu
		//WidgetInstance->SetVisibility(EVisibility::Hidden);
	}
}


float ATheStationCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser)
{
	float actualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	CurHealth -= actualDamage;
	if (CurHealth < 0)
	{

		UGameplayStatics::OpenLevel(GetWorld(), FName(*UGameplayStatics::GetCurrentLevelName(GetWorld())));
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, "U DED GAME OVER");*/
	}

	return actualDamage;
}

void ATheStationCharacter::AddHealth(int amount)
{
	CurHealth += amount;
	if (CurHealth > MaxHealth)
		CurHealth = MaxHealth;
}

void ATheStationCharacter::AddAmmo(int amount)
{
	ammoPool += amount;
}

void ATheStationCharacter::OnReload()
{
	if (ammoPool <= 0 || loadedAmmo >= maxLoadedAmmo)
		return;

	canShoot = false;

	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer(UnusedHandle, this, &ATheStationCharacter::ReloadFinished, reloadTime, false);
}

void ATheStationCharacter::ReloadFinished()
{
	if (ammoPool < (maxLoadedAmmo - loadedAmmo))
	{
		loadedAmmo += ammoPool;
		ammoPool = 0;
	}
	else
	{
		ammoPool = ammoPool - (maxLoadedAmmo - loadedAmmo);
		loadedAmmo = maxLoadedAmmo;
	}
	canShoot = true;
}

void ATheStationCharacter::OnFire()
{ 
	if (loadedAmmo <= 0 || !canShoot)
		return;

	loadedAmmo -= 1;

	FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
	RV_TraceParams.bTraceComplex = true;
	//TODO: Find what to do with this?
	//RV_TraceParams.bTraceAsyncScene = true;
	RV_TraceParams.bReturnPhysicalMaterial = false;
	//RV_TraceParams.AddIgnoredActor(this);

	//Re-initialize hit info
	FHitResult RV_Hit(ForceInit);

	const FRotator SpawnRotation = FirstPersonCameraComponent->GetComponentRotation();
	const FVector SpawnLocation = FP_Gun->GetComponentLocation();//VR_MuzzeLocation->GetComponentLocation(); //((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

	
	FVector Start = SpawnLocation;
	FVector End = SpawnLocation + (SpawnRotation.Vector() * 2500);

	GetWorld()->LineTraceSingleByChannel(
		RV_Hit,        //result
		Start,    //start
		End, //end
		ECC_Pawn, //collision channel
		RV_TraceParams
	);

	/*DrawDebugLine(
		GetWorld(),
		Start,
		End,
		FColor(255, 6, 0),
		false, 3, 0,
		12.333
	);*/

	muzzleFlash->Activate();
	//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), muzzleFlash, SpawnLocation, SpawnRotation, true);

	if (RV_Hit.bBlockingHit) 
	{
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, RV_Hit.GetActor()->GetName());*/

		if (RV_Hit.GetActor()->IsA(AEnemyCharacter::StaticClass()))
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), bloodParticle, RV_Hit.ImpactPoint, RV_Hit.GetActor()->GetActorRotation(), true)->SetRelativeScale3D(FVector(4,4,4));
		}
		else
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticle, RV_Hit.ImpactPoint, RV_Hit.GetActor()->GetActorRotation(), true);
		}	

		TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
		FDamageEvent DamageEvent(ValidDamageTypeClass);

		RV_Hit.GetActor()->TakeDamage(bulletDamage, DamageEvent, GetController(), this);

		//UE_LOG(LogTemp, Warning, TEXT(RV_Hit.GetActor()));
		/*if (RV_Hit.GetActor()->IsA(AMyProjectCharacter::StaticClass()))
		{
			AMyProjectCharacter* HitCharacter = Cast(RV_Hit.GetActor());
			if (!HitCharacter) 
				return;

			//do stuff to the HitCharacter
		}*/
	}

	// try and fire a projectile
	/*if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<ATheStationProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<ATheStationProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		}
	}*/

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	/*if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}*/
}

void ATheStationCharacter::OnBeginShoot()
{
	shooting = true;
}

void ATheStationCharacter::OnEndShoot()
{
	shooting = false;
}

void ATheStationCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATheStationCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void ATheStationCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void ATheStationCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void ATheStationCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ATheStationCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ATheStationCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATheStationCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool ATheStationCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATheStationCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &ATheStationCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ATheStationCharacter::TouchUpdate);
	}
	return bResult;
}