// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "TheStationGameMode.generated.h"

UCLASS(minimalapi)
class ATheStationGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATheStationGameMode();

	virtual void BeginPlay() override;

	//void ChoosePlayerStart() override;
};



