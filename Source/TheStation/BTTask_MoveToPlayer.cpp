// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyAI.h"
#include "TheStationCharacter.h"
#include "EnemyCharacter.h"
#include "BTTask_MoveToPlayer.h"


EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AEnemyAI* CharPC = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

	ATheStationCharacter *Enemy = Cast<ATheStationCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->EnemyKeyID));

	AEnemyCharacter* thisAI = Cast<AEnemyCharacter>(CharPC->GetPawn());

	bool IsSeeingEnemy = OwnerComp.GetBlackboardComponent()->GetValueAsBool("IsSeeingEnemy");

	float distanceToPlayer;
	float attackTimer = 1.5f;
	if(Enemy)
		distanceToPlayer = (thisAI->GetActorLocation() - Enemy->GetActorLocation()).Size();

	if (IsSeeingEnemy && Enemy && distanceToPlayer > 150)
	{
		if (!thisAI->playedDetectSound)
		{
			thisAI->playedDetectSound = true;
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), thisAI->SpotSound, thisAI->GetActorLocation());
		}
		
		thisAI->lastEnemyPos = Enemy->GetActorLocation();
		CharPC->MoveToActor(Enemy, 150, true, true, true, 0, true);
		return EBTNodeResult::Succeeded;
	}
	else if (IsSeeingEnemy && Enemy && distanceToPlayer < 150 && canAttack)
	{
		canAttack = false;
		//attack player
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, TEXT("Attacking player"));*/

		TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
		FDamageEvent DamageEvent(ValidDamageTypeClass);

		Enemy->TakeDamage(thisAI->damage, DamageEvent, CharPC, thisAI);

		UGameplayStatics::PlaySoundAtLocation(GetWorld(), thisAI->AttackSound, thisAI->GetActorLocation());

		FTimerHandle UnusedHandle;
		GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &UBTTask_MoveToPlayer::AttackReady, attackTimer, false);

		return EBTNodeResult::Succeeded;
	}
	else
	{
		CharPC->MoveToLocation(thisAI->lastEnemyPos, 150, true, true, true, 0);
		thisAI->playedDetectSound = false;
		//CharPC->StopMovement();
		return EBTNodeResult::Succeeded;
	}
		
}

void UBTTask_MoveToPlayer::AttackReady()
{
	canAttack = true;
}

