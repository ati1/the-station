// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "MainMenu.generated.h"

/**
 * 
 */
UCLASS()
class THESTATION_API AMainMenu : public AGameModeBase
{
	GENERATED_BODY()
	
public:
		UFUNCTION(BlueprintCallable, Category = "HasKey")
		void SetHasKey(bool val);
	
};
