// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "TheStationCharacter.h"
#include "HealthPickup.h"


// Sets default values
AHealthPickup::AHealthPickup()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	hAmount = 15;
	TouchSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TouchSphereComponent"));
	TouchSphere->SetSphereRadius(45, false);
	TouchSphere->OnComponentBeginOverlap.AddDynamic(this, &AHealthPickup::OnOverlapBegin);
	RootComponent = TouchSphere;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AHealthPickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHealthPickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AHealthPickup::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ATheStationCharacter* player = Cast<ATheStationCharacter>(OtherActor);

	if (player)
	{
		player->AddHealth(hAmount);
		this->Destroy();
	}
}

