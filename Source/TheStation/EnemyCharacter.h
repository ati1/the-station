// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class THESTATION_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly)
	float MaxHealth = 100;
	float CurHealth;


public:
	// Sets default values for this character's properties
	AEnemyCharacter();

	FVector lastEnemyPos;

	bool playedDetectSound;

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;

	UPROPERTY(EditDefaultsOnly)
	float damage = 25;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
	USoundCue* AttackSound;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
	USoundCue* SpotSound;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
	USoundCue* DieSound;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

};
