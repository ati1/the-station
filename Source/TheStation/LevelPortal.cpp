// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "TheStationCharacter.h"
#include "LevelPortal.h"


// Sets default values
ALevelPortal::ALevelPortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	Trigger = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerCollider"));
	RootComponent = Trigger;

	Trigger->SetGenerateOverlapEvents(true);
	Trigger->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

}

// Called when the game starts or when spawned
void ALevelPortal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelPortal::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ALevelPortal::NotifyActorBeginOverlap(AActor* Other)
{
	if (Other->GetClass()->IsChildOf(ATheStationCharacter::StaticClass()))
	{
		ATheStationCharacter* player = Cast<ATheStationCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (player)
		{
			//Loadlevel
			UGameplayStatics::OpenLevel(GetWorld(), FName(*LevelToLoad));

		}
	}
}

