// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "TheStationCharacter.h"
#include "Door.h"


// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	Trigger = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerCollider"));
	RootComponent = Trigger;

	Trigger->SetGenerateOverlapEvents(true);
	Trigger->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	mesh->SetCanEverAffectNavigation(false);
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADoor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ADoor::OpenDoor()
{
	if (!isOpen)
	{
		mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision); // SetActorEnableCollision(false);
		RootComponent->SetVisibility(false, true);
		isOpen = true;
	}
	else
	{
		mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		RootComponent->SetVisibility(true, true);
		isOpen = false;
	}
}

void ADoor::NotifyActorBeginOverlap(AActor* Other)
{
	//enable widget in ui to show
	if(Other->GetClass()->IsChildOf(ATheStationCharacter::StaticClass())) 
	{
		ATheStationCharacter* player = Cast<ATheStationCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (player)
		{
			player->door = this;
			player->showInteractMessage = true;
		}
	}
}

void ADoor::NotifyActorEndOverlap(AActor* Other)
{
	//hide widget in ui
	if (Other->GetClass()->IsChildOf(ATheStationCharacter::StaticClass()))
	{
		ATheStationCharacter* player = Cast<ATheStationCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (player)
		{
			player->door = nullptr;
			player->showInteractMessage = false;
		}
	}
}

