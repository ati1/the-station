// Fill out your copyright notice in the Description page of Project Settings.

#include "TheStation.h"
#include "EnemyCharacter.h"


// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurHealth = MaxHealth;
	lastEnemyPos = GetActorLocation();
}

// Called every frame
void AEnemyCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AEnemyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser)
{
	float actualDmg = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	CurHealth -= actualDmg;

	if (CurHealth <= 0)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), DieSound, GetActorLocation());
		Destroy();
	}
		

	return actualDmg;
}


